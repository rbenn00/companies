require 'test_helper'

class Spree::CompaniesControllerTest < ActionController::TestCase
  setup do
    @spree_company = spree_companies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:spree_companies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create spree_company" do
    assert_difference('Spree::Company.count') do
      post :create, spree_company: { contact: @spree_company.contact, email: @spree_company.email, logo: @spree_company.logo, name: @spree_company.name, phone: @spree_company.phone, website: @spree_company.website }
    end

    assert_redirected_to spree_company_path(assigns(:spree_company))
  end

  test "should show spree_company" do
    get :show, id: @spree_company
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @spree_company
    assert_response :success
  end

  test "should update spree_company" do
    put :update, id: @spree_company, spree_company: { contact: @spree_company.contact, email: @spree_company.email, logo: @spree_company.logo, name: @spree_company.name, phone: @spree_company.phone, website: @spree_company.website }
    assert_redirected_to spree_company_path(assigns(:spree_company))
  end

  test "should destroy spree_company" do
    assert_difference('Spree::Company.count', -1) do
      delete :destroy, id: @spree_company
    end

    assert_redirected_to spree_companies_path
  end
end
