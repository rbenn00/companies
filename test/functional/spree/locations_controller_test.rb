require 'test_helper'

class Spree::LocationsControllerTest < ActionController::TestCase
  setup do
    @spree_location = spree_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:spree_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create spree_location" do
    assert_difference('Spree::Location.count') do
      post :create, spree_location: { address: @spree_location.address, city: @spree_location.city, company_id: @spree_location.company_id, email: @spree_location.email, fax: @spree_location.fax, latitude: @spree_location.latitude, longitude: @spree_location.longitude, phone: @spree_location.phone, postal_code: @spree_location.postal_code, state: @spree_location.state, website: @spree_location.website }
    end

    assert_redirected_to spree_location_path(assigns(:spree_location))
  end

  test "should show spree_location" do
    get :show, id: @spree_location
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @spree_location
    assert_response :success
  end

  test "should update spree_location" do
    put :update, id: @spree_location, spree_location: { address: @spree_location.address, city: @spree_location.city, company_id: @spree_location.company_id, email: @spree_location.email, fax: @spree_location.fax, latitude: @spree_location.latitude, longitude: @spree_location.longitude, phone: @spree_location.phone, postal_code: @spree_location.postal_code, state: @spree_location.state, website: @spree_location.website }
    assert_redirected_to spree_location_path(assigns(:spree_location))
  end

  test "should destroy spree_location" do
    assert_difference('Spree::Location.count', -1) do
      delete :destroy, id: @spree_location
    end

    assert_redirected_to spree_locations_path
  end
end
