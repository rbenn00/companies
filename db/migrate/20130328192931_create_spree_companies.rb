class CreateSpreeCompanies < ActiveRecord::Migration
  def change
    create_table :spree_companies do |t|
      t.string :name
      t.string :contact
      t.string :phone
      t.string :email
      t.string :website
      t.string :logo

      t.timestamps
    end
  end
end
