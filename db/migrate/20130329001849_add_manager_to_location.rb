class AddManagerToLocation < ActiveRecord::Migration
  def change
    add_column :spree_locations, :manager, :string
  end
end
