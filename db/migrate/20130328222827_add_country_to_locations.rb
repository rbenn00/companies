class AddCountryToLocations < ActiveRecord::Migration
  def change
    add_column :spree_locations, :country, :string
  end
end
