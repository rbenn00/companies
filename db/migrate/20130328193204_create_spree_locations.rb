class CreateSpreeLocations < ActiveRecord::Migration
  def change
    create_table :spree_locations do |t|
      t.integer :company_id
      t.string :address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :phone
      t.string :fax
      t.string :email
      t.string :website
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps
    end
  end
end
