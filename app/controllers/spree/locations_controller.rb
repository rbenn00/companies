class Spree::LocationsController < ApplicationController
  # GET /spree/locations
  # GET /spree/locations.json
  def index
    @spree_locations = Spree::Location.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @spree_locations }
    end
  end

  # GET /spree/locations/1
  # GET /spree/locations/1.json
  def show
    @spree_location = Spree::Location.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @spree_location }
    end
  end

  # GET /spree/locations/new
  # GET /spree/locations/new.json
  def new
    @spree_location = Spree::Location.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @spree_location }
    end
  end

  # GET /spree/locations/1/edit
  def edit
    @spree_location = Spree::Location.find(params[:id])
  end

  # POST /spree/locations
  # POST /spree/locations.json
  def create
    @spree_location = Spree::Location.new(params[:spree_location])

    respond_to do |format|
      if @spree_location.save
        format.html { redirect_to @spree_location, notice: 'Location was successfully created.' }
        format.json { render json: @spree_location, status: :created, location: @spree_location }
      else
        format.html { render action: "new" }
        format.json { render json: @spree_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /spree/locations/1
  # PUT /spree/locations/1.json
  def update
    @spree_location = Spree::Location.find(params[:id])

    respond_to do |format|
      if @spree_location.update_attributes(params[:spree_location])
        format.html { redirect_to @spree_location, notice: 'Location was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @spree_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spree/locations/1
  # DELETE /spree/locations/1.json
  def destroy
    @spree_location = Spree::Location.find(params[:id])
    @spree_location.destroy

    respond_to do |format|
      format.html { redirect_to spree_locations_url }
      format.json { head :no_content }
    end
  end
end
