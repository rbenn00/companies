class Spree::CompaniesController < ApplicationController
  # GET /spree/companies
  # GET /spree/companies.json
  def index
    @spree_companies = Spree::Company.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @spree_companies }
    end
  end

  # GET /spree/companies/1
  # GET /spree/companies/1.json
  def show
    @spree_company = Spree::Company.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @spree_company }
    end
  end

  # GET /spree/companies/new
  # GET /spree/companies/new.json
  def new
    @spree_company = Spree::Company.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @spree_company }
    end
  end

  # GET /spree/companies/1/edit
  def edit
    @spree_company = Spree::Company.find(params[:id])
  end

  # POST /spree/companies
  # POST /spree/companies.json
  def create
    @spree_company = Spree::Company.new(params[:spree_company])

    respond_to do |format|
      if @spree_company.save
        format.html { redirect_to @spree_company, notice: 'Company was successfully created.' }
        format.json { render json: @spree_company, status: :created, location: @spree_company }
      else
        format.html { render action: "new" }
        format.json { render json: @spree_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /spree/companies/1
  # PUT /spree/companies/1.json
  def update
    @spree_company = Spree::Company.find(params[:id])

    respond_to do |format|
      if @spree_company.update_attributes(params[:spree_company])
        format.html { redirect_to @spree_company, notice: 'Company was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @spree_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spree/companies/1
  # DELETE /spree/companies/1.json
  def destroy
    @spree_company = Spree::Company.find(params[:id])
    @spree_company.destroy

    respond_to do |format|
      format.html { redirect_to spree_companies_url }
      format.json { head :no_content }
    end
  end
end
