class Spree::Location < ActiveRecord::Base
  belongs_to :company
  attr_accessible :address, :city, :company_id, :email, :fax, :latitude, :longitude, :phone, :postal_code, :state, :website
  

end
