class Spree::Company < ActiveRecord::Base
  attr_accessible :contact, :email, :logo, :name, :phone, :website
  attr_accessible :locations_attributes
  
  has_many :locations
  accepts_nested_attributes_for :locations,
                                :allow_destroy => true
                                
 # validates_associated :locations                              
  
end
